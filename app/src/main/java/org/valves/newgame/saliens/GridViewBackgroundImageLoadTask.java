package org.valves.newgame.saliens;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.GridView;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class GridViewBackgroundImageLoadTask extends AsyncTask<Void, Void, Bitmap> {
    private Context ctx;
    private String url;
    private GridView gridView;

    public GridViewBackgroundImageLoadTask(Context ctx, String url, GridView gridView) {
        this.ctx = ctx;
        this.url = url;
        this.gridView = gridView;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        try {
            URL urlConnection = new URL("https://steamcdn-a.akamaihd.net/steamcommunity/public/assets/saliengame/maps/" + url);
            HttpsURLConnection connection = (HttpsURLConnection) urlConnection
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        Drawable drawable = new BitmapDrawable(ctx.getResources(), result);
        gridView.setBackground(drawable);
    }
}