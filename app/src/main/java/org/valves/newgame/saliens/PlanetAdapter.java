package org.valves.newgame.saliens;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PlanetAdapter extends BaseAdapter {
    private Context context;
    private JSONObject planetData;

    public PlanetAdapter(Context context, JSONObject planetData) {
        this.context = context;
        this.planetData = planetData;
    }

    public int getCount() {
        return 96;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(context);
            imageView.setLayoutParams(new ViewGroup.LayoutParams(64, 64));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(1, 1, 1, 1);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setAdjustViewBounds(true);

        try {
            JSONObject zone = planetData.getJSONArray("zones").getJSONObject(position);
            if (zone.getBoolean("captured")) {
                String avatarID = zone.getJSONObject("leader").getString("avatar");
                String path = "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/" +
                        avatarID.substring(0, 2) + "/" +
                        avatarID + ".jpg";
                new ImageLoadTask(path, imageView).execute();
            } else {
                if (zone.getInt("difficulty") == 1) {
                    imageView.setImageResource(R.mipmap.uncaptured_low);
                } else if (zone.getInt("difficulty") == 2) {
                    imageView.setImageResource(R.mipmap.uncaptured_medium);
                } else if (zone.getInt("difficulty") == 3) {
                    imageView.setImageResource(R.mipmap.uncaptured_high);
                } else if (zone.getInt("difficulty") == 4) {
                    imageView.setImageResource(R.mipmap.uncaptured_boss);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return imageView;
    }
}