package org.valves.newgame.saliens;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class PlanetView extends AppCompatActivity {
    //12x8
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.planet_view);

        Bundle b = getIntent().getExtras();
        JSONObject planetData = null;
        try {
            if (b != null) {
                planetData = new JSONObject(b.getString("planetData"));

                GridView grid = (GridView) findViewById(R.id.planetGrid);
                final JSONObject planet = planetData.getJSONObject("response") //risky boi for grid
                        .getJSONArray("planets")
                        .getJSONObject(0);
                grid.setAdapter(new PlanetAdapter(this, planet));

                new GridViewBackgroundImageLoadTask(this, planet.getJSONObject("state")
                                                                     .getString("map_filename"),
                                                               grid).execute();

                new GameDetailsLoadTask(this, planet).execute();

                grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                        try {
                            JSONObject zone = planet.getJSONArray("zones").getJSONObject(arg2);
                            ProgressBar tileProgress = findViewById(R.id.tileProgress);
                            TextView tileScore = findViewById(R.id.tileScore);

                            String diff = "";
                            switch (zone.getInt("difficulty")) {
                                case 1:
                                    diff = "easy";
                                    break;
                                case 2:
                                    diff = "medium";
                                    break;
                                case 3:
                                    diff = "hard";
                                    break;
                                case 4:
                                    diff = "boss";
                                    break;
                            }

                            if (zone.getBoolean("captured")) {
                                tileProgress.setProgress(100);
                                tileScore.setText("Captured 100%  Difficulty " + diff);
                            } else {
                                int percentage = (int)Math.floor(zone.getDouble("capture_progress")*100);
                                tileProgress.setProgress(percentage);
                                tileScore.setText("Uncaptured " + percentage + "%  Difficulty " + diff);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public static class GameDetailsLoadTask extends AsyncTask<String, Void, Void> {
        private WeakReference<PlanetView> activityReference;
        private JSONObject planet;

        private ArrayList<String> giveawayNames;
        private ArrayList<String> giveawayPrices;
        private ArrayList<String> giveawayImageURLs;

        GameDetailsLoadTask(PlanetView context, JSONObject planet) {
            activityReference = new WeakReference<>(context);
            this.planet = planet;
        }

        @Override
        protected Void doInBackground(String... objects) {
            JSONArray giveaways = null;
            try {
                giveaways = planet.getJSONArray("giveaway_apps");
                giveawayNames = new ArrayList<>();
                giveawayPrices = new ArrayList<>();
                giveawayImageURLs = new ArrayList<>();
                for (int i = 0; i < 4; i++) {
                    JSONObject info = new JSONObject(GetSteamInfo(Integer.toString(giveaways.getInt(i))));
                    JSONObject data = info.getJSONObject(Integer.toString(giveaways.getInt(i)))
                            .getJSONObject("data");
                    JSONObject price = data.getJSONObject("price_overview");
                    giveawayNames.add(data.getString("name"));
                    giveawayPrices.add("$" + AddDecimal(price.getString("initial")) + " -> $" +
                            AddDecimal(price.getString("final")));
                    giveawayImageURLs.add(data.getString("header_image"));
                }
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void param) {
            final PlanetView ma = activityReference.get(); //risky boi for onclicks
            ImageView gi1 = ma.findViewById(R.id.gameImage1);
            ImageView gi2 = ma.findViewById(R.id.gameImage2);
            ImageView gi3 = ma.findViewById(R.id.gameImage3);
            ImageView gi4 = ma.findViewById(R.id.gameImage4);
            TextView dt1 = ma.findViewById(R.id.gameDetails1);
            TextView dt2 = ma.findViewById(R.id.gameDetails2);
            TextView dt3 = ma.findViewById(R.id.gameDetails3);
            TextView dt4 = ma.findViewById(R.id.gameDetails4);
            new ImageLoadTask(giveawayImageURLs.get(0), gi1, false).execute();
            new ImageLoadTask(giveawayImageURLs.get(1), gi2, false).execute();
            new ImageLoadTask(giveawayImageURLs.get(2), gi3, false).execute();
            new ImageLoadTask(giveawayImageURLs.get(3), gi4, false).execute();
            dt1.setText(giveawayNames.get(0) + "\n" + giveawayPrices.get(0));
            dt2.setText(giveawayNames.get(1) + "\n" + giveawayPrices.get(1));
            dt3.setText(giveawayNames.get(2) + "\n" + giveawayPrices.get(2));
            dt4.setText(giveawayNames.get(3) + "\n" + giveawayPrices.get(3));
        }

        //ironically word for... *ahem* character by character
        //https://stackoverflow.com/questions/5884353/insert-a-character-in-a-string-at-a-certain-position
        private String AddDecimal(String price) {
            return new StringBuilder(price).insert(price.length()-2, ".").toString();
        }
    }

    public static String GetSteamInfo(String id) throws IOException {
        URL url = new URL("https://store.steampowered.com/api/appdetails/?appids=" + id + "&cc=us");
        URLConnection urlConnection = url.openConnection();
        HttpsURLConnection httpConnection = (HttpsURLConnection)urlConnection;
        httpConnection.setRequestMethod("GET");
        httpConnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");
        httpConnection.setRequestProperty("Accept", "*/*");
        httpConnection.setRequestProperty("Origin", "https://steamcommunity.com");
        httpConnection.setRequestProperty("Referer", "https://steamcommunity.com/saliengame/play");
        httpConnection.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3464.0 Safari/537.36");
        BufferedReader in = new BufferedReader(new InputStreamReader(httpConnection.getInputStream(), "UTF8"));
        if (httpConnection.getResponseCode() != 200)
            return null;
        StringBuilder result = new StringBuilder();
        String line;
        while ((line = in.readLine()) != null) {
            result.append(line);
        }
        return result.toString();
    }
}
