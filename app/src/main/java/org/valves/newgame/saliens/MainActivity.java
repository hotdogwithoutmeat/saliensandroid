package org.valves.newgame.saliens;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    public String webApiHost;
    public String webApiHostSecure;
    public String token;
    public String steamID;
    public String personaName;

    public JSONObject playerData;
    public JSONArray planetData;

    public int scoreCur = 0;
    public int scoreNex = 0;
    public int level = 0;

    public boolean notificationOn = false;
    public int notificationId = 1234;

    public NotificationManagerCompat notificationManager;
    public NotificationCompat.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        TextView ib = findViewById(R.id.infobox);
        File file = getFileStreamPath("saliens.json");
        if (!file.exists()) {
            ib.setText("Further setup required");
            AlertDialog.Builder alert = new AlertDialog.Builder(this);

            alert.setTitle("Log in and paste the json from");
            alert.setMessage("https://steamcommunity.com/saliengame/gettoken");

            final EditText input = new EditText(this);
            alert.setView(input);

            alert.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String value = input.getText().toString();
                    File file = MainActivity.this.getFileStreamPath("saliens.json");
                    FileOutputStream stream = null;
                    try {
                        stream = new FileOutputStream(file);
                        stream.write(value.getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            stream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

                    alert.setTitle("Saliens Android Client");
                    alert.setMessage("Please restart to make changes");

                    alert.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            finishAndRemoveTask();
                        }
                    });

                    alert.show();
                }
            });

            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    finishAndRemoveTask();
                }
            });

            alert.show();
        } else {
            LoadMenu(file);
        }
    }

    public void LoadMenu(File file) {
        FileInputStream stream = null;
        try {
            stream = new FileInputStream(file);
            StringBuffer fileContent = new StringBuffer("");

            byte[] dataBuffer = new byte[1024];
            int i;

            while ((i = stream.read(dataBuffer)) != -1) {
                fileContent.append(new String(dataBuffer, 0, i));
            }
            String data = fileContent.toString();
            new GetMenuInfo(this).execute(data);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static class GetMenuInfo extends AsyncTask<String, Void, Void> {
        private WeakReference<MainActivity> activityReference;
        private String playerInfo;

        private String webApiHost;
        private String webApiHostSecure;
        private String token;
        private String steamID;
        private String personaName;

        private ArrayList<String> planetImageList;
        private ArrayList<String> planetNameList;
        private ArrayList<Double> planetProgress;
        private ArrayList<String> tileData;

        private int timeInZone;
        private boolean shouldWait;

        private JSONObject playerData;
        private JSONArray planetData;
        private String playerDataStr;
        private String planetDataStr;

        GetMenuInfo(MainActivity context) {
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected Void doInBackground(String... objects) {
            JSONObject jObject = null;
            TextView ib = activityReference.get().findViewById(R.id.infobox);
            try {
                jObject = new JSONObject(objects[0]);
                webApiHost = jObject.getString("webapi_host");
                webApiHostSecure = jObject.getString("webapi_host_secure");
                token = jObject.getString("token");
                steamID = jObject.getString("steamid");
                personaName = jObject.getString("persona_name");
                int success = jObject.getInt("success");
                if (success == 1) {
                    ib.setText("Successfully logged in as " + personaName + "\n");
                    /*playerInfo = Post(webApiHostSecure,
                            "ITerritoryControlMinigameService/GetPlayerInfo",
                            "access_token=" + token);*/
                    playerDataStr = Post("ITerritoryControlMinigameService/GetPlayerInfo",
                            "access_token=" + token);
                    playerData = new JSONObject(playerDataStr).getJSONObject("response");
                    playerInfo = "";
                    if (playerData.has("active_planet")) {
                        playerInfo += "Current Planet ID: " + playerData.getInt("active_planet") + "\n";
                        playerInfo += "Time: " + SecondsToTime(playerData.getInt("time_on_planet")) + "\n";
                        publishProgress();
                    }
                    if (playerData.has("clan_info"))
                        playerInfo += "Group: " + playerData.getJSONObject("clan_info").getString("name") + "\n";
                    publishProgress();
                    playerInfo += "Level: " + playerData.getInt("level") + " (" +
                                              playerData.getInt("score") + "/" +
                                              playerData.getInt("next_level_score") + ")" + "\n";
                    publishProgress();
                    /*if (playerData.has("active_planet")) {
                        playerInfo += "Leaving planet..." + "\n";
                        Post("IMiniGameService/LeaveGame",
                              "access_token=" + token +
                                    "&gameid=" + playerData.getInt("active_planet"));
                    }*/
                    if (playerData.has("time_in_zone")) {
                        timeInZone = playerData.getInt("time_in_zone");
                        if (timeInZone < 110) {
                            playerInfo += "Currently in a game... time left is " + (110-timeInZone) + "\n";
                            shouldWait = true;
                            publishProgress();
                        } else {
                            shouldWait = false;
                        }
                    } else {
                        shouldWait = false;
                    }
                    playerInfo += "Checking planets...\n";
                    publishProgress();
                    planetDataStr = Get("ITerritoryControlMinigameService/GetPlanets",
                            "active_only=1&language=english");
                    planetData = new JSONObject(planetDataStr).getJSONObject("response").getJSONArray("planets");
                    playerInfo += "Found " + planetData.length() + " planets\n";
                    publishProgress();
                    planetImageList = new ArrayList<>();
                    planetNameList = new ArrayList<>();
                    planetProgress = new ArrayList<>();
                    tileData = new ArrayList<>();
                    for (int i = 0; i < planetData.length(); i++) {
                        JSONObject state = planetData.getJSONObject(i).getJSONObject("state");
                        playerInfo += planetData.getJSONObject(i).getJSONObject("state").getString("name") + "\n";
                        publishProgress();
                        planetImageList.add("https://steamcdn-a.akamaihd.net/steamcommunity/public/assets/saliengame/planets/" +
                                state.getString("image_filename"));
                        planetNameList.add(state.getString("name"));
                        planetProgress.add(state.getDouble("capture_progress"));
                        tileData.add(Get("ITerritoryControlMinigameService/GetPlanet",
                                "id=" + planetData.getJSONObject(i).getInt("id") + "&language=english"));
                    }
                }
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... param) {
            MainActivity ma = activityReference.get();
            TextView ib = ma.findViewById(R.id.infobox);
            ib.append(playerInfo);
            playerInfo = "";
        }

        @Override
        protected void onPostExecute(Void param) {
            final MainActivity ma = activityReference.get(); //risky boi for onclicks
            TextView ib = ma.findViewById(R.id.infobox);
            ib.append(playerInfo);
            ma.webApiHost = webApiHost;
            ma.webApiHostSecure = webApiHostSecure;
            ma.token = token;
            ma.steamID = steamID;
            ma.personaName = personaName;
            ma.playerData = playerData;
            ma.planetData = planetData;

            try {
                ma.level = playerData.getInt("level");
                ma.scoreCur = playerData.getInt("score");
                ma.scoreNex = playerData.getInt("next_level_score");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (planetImageList.size() < 5 || planetNameList.size() < 5) {
                return;
            }

            ImageButton pb1 = ma.findViewById(R.id.planetJump1);
            ImageButton pb2 = ma.findViewById(R.id.planetJump2);
            ImageButton pb3 = ma.findViewById(R.id.planetJump3);
            ImageButton pb4 = ma.findViewById(R.id.planetJump4);
            ImageButton pb5 = ma.findViewById(R.id.planetJump5);

            TextView pt1 = ma.findViewById(R.id.planetDetails1);
            TextView pt2 = ma.findViewById(R.id.planetDetails2);
            TextView pt3 = ma.findViewById(R.id.planetDetails3);
            TextView pt4 = ma.findViewById(R.id.planetDetails4);
            TextView pt5 = ma.findViewById(R.id.planetDetails5);

            ProgressBar pp1 = ma.findViewById(R.id.planetProgress1);
            ProgressBar pp2 = ma.findViewById(R.id.planetProgress2);
            ProgressBar pp3 = ma.findViewById(R.id.planetProgress3);
            ProgressBar pp4 = ma.findViewById(R.id.planetProgress4);
            ProgressBar pp5 = ma.findViewById(R.id.planetProgress5);

            new ImageLoadTask(planetImageList.get(0), pb1).execute();
            new ImageLoadTask(planetImageList.get(1), pb2).execute();
            new ImageLoadTask(planetImageList.get(2), pb3).execute();
            new ImageLoadTask(planetImageList.get(3), pb4).execute();
            new ImageLoadTask(planetImageList.get(4), pb5).execute();

            pt1.setText(planetNameList.get(0));
            pt2.setText(planetNameList.get(1));
            pt3.setText(planetNameList.get(2));
            pt4.setText(planetNameList.get(3));
            pt5.setText(planetNameList.get(4));

            pp1.setProgress((int)Math.floor(planetProgress.get(0)*100));
            pp2.setProgress((int)Math.floor(planetProgress.get(1)*100));
            pp3.setProgress((int)Math.floor(planetProgress.get(2)*100));
            pp4.setProgress((int)Math.floor(planetProgress.get(3)*100));
            pp5.setProgress((int)Math.floor(planetProgress.get(4)*100));

            pb1.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ma, PlanetView.class);
                    intent.putExtra("planetData", tileData.get(0));
                    ma.startActivity(intent);
                }
            });

            pb2.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ma, PlanetView.class);
                    intent.putExtra("planetData", tileData.get(1));
                    ma.startActivity(intent);
                }
            });

            pb3.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ma, PlanetView.class);
                    intent.putExtra("planetData", tileData.get(2));
                    ma.startActivity(intent);
                }
            });

            pb4.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ma, PlanetView.class);
                    intent.putExtra("planetData", tileData.get(3));
                    ma.startActivity(intent);
                }
            });

            pb5.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ma, PlanetView.class);
                    intent.putExtra("planetData", tileData.get(4));
                    ma.startActivity(intent);
                }
            });

            if (shouldWait) {
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    public void run() {
                        ma.FindBestPlanetAndAttack(playerData, planetData, tileData);
                    }
                }, (110-timeInZone)*1000);
            } else {
                ma.FindBestPlanetAndAttack(playerData, planetData, tileData);
            }

            super.onPostExecute(param);
        }

        private static String SecondsToTime(int sec) {
            if (sec >= 3600) {
                return Double.toString(Math.floor(sec/3600)) + " hr " +
                       Double.toString(Math.floor((sec%3600)/60)) + " min " +
                       Double.toString(Math.floor(sec%60)) + " sec";
            } else if (sec >= 60) {
                return Double.toString(Math.floor((sec%3600)/60)) + " min " +
                       Double.toString(Math.floor(sec%60)) + " sec";
            } else {
                return Double.toString(Math.floor(sec%60)) + " sec";
            }
        }
    }

    public static class RestartAndGetNewInfo extends AsyncTask<Void, Void, Void> {
        private WeakReference<MainActivity> activityReference;

        private ArrayList<String> tileData;

        private JSONObject playerData;
        private JSONArray planetData;
        private String playerDataStr;
        private String planetDataStr;

        RestartAndGetNewInfo(MainActivity context) {
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected Void doInBackground(Void... objects) {
            JSONObject jObject = null;
            try {
                playerDataStr = Post("ITerritoryControlMinigameService/GetPlayerInfo",
                        "access_token=" + activityReference.get().token);
                playerData = new JSONObject(playerDataStr).getJSONObject("response");
                if (playerData.has("active_zone_game")) {
                    Post("IMiniGameService/LeaveGame",
                            "access_token=" + activityReference.get().token +
                                    "&gameid=" + playerData.getInt("active_zone_game"));
                }
                playerData = new JSONObject(playerDataStr).getJSONObject("response");
                /*if (playerData.has("active_planet")) {
                    Post("IMiniGameService/LeaveGame",
                            "access_token=" + token +
                                    "&gameid=" + playerData.getInt("active_planet"));
                }*/
                planetDataStr = Get("ITerritoryControlMinigameService/GetPlanets",
                        "active_only=1&language=english");
                planetData = new JSONObject(planetDataStr).getJSONObject("response").getJSONArray("planets");
                tileData = new ArrayList<>();
                for (int i = 0; i < planetData.length(); i++) {
                    JSONObject state = planetData.getJSONObject(i).getJSONObject("state");
                    tileData.add(Get("ITerritoryControlMinigameService/GetPlanet",
                            "id=" + planetData.getJSONObject(i).getInt("id") + "&language=english"));
                }
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void param) {
            MainActivity ma = activityReference.get();

            try {
                ma.level = playerData.getInt("level");
                ma.scoreCur = playerData.getInt("score");
                ma.scoreNex = playerData.getInt("next_level_score");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            ma.FindBestPlanetAndAttack(playerData, planetData, tileData);

            super.onPostExecute(param);
        }
    }

    public void FindBestPlanetAndAttack(JSONObject playerData, JSONArray planetData, ArrayList<String> tileData) {
        try {
            /*int[] rankings = new int[] {0,0,0,0,0};
            int smallestPlayerCount = Integer.MAX_VALUE;
            int smallestPlayerIndex = -1;
            for (int i = 0; i < 5; i++) {
                if (planetData.getJSONObject(i).getJSONObject("state").has("current_players"))
                    continue;
                int currentPlayers = planetData.getJSONObject(i)
                                               .getJSONObject("state")
                                               .getInt("current_players");
                if (currentPlayers < smallestPlayerCount) {
                    smallestPlayerCount = currentPlayers;
                    smallestPlayerIndex = i;
                }
            }
            if (smallestPlayerIndex != -1)
                rankings[smallestPlayerIndex] += 1;

            double smallestCapturePercent = Double.MAX_VALUE;
            int smallestCaptureIndex = -1;
            for (int i = 0; i < 5; i++) {
                if (planetData.getJSONObject(i).getJSONObject("state").has("capture_progress"))
                    continue;
                double currentProgress = planetData.getJSONObject(i)
                                                   .getJSONObject("state")
                                                   .getDouble("capture_progress");
                if (currentProgress < smallestCapturePercent) {
                    smallestCapturePercent = currentProgress;
                    smallestCaptureIndex = i;
                }
            }
            if (smallestCaptureIndex != -1)
                rankings[smallestCaptureIndex] += 1;*/

            int highestBossCount = 0;
            int highestBossCountIndex = -1;
            int highestHardCount = 0;
            int highestHardCountIndex = -1;
            int highestMediumCount = 0;
            int highestMediumCountIndex = -1;
            int highestEasyCount = 0;
            int highestEasyCountIndex = -1;
            for (int i = 0; i < 5; i++) {
                JSONObject planetInfo = new JSONObject(tileData.get(i));
                JSONArray zones = planetInfo.getJSONObject("response")
                                            .getJSONArray("planets")
                                            .getJSONObject(0)
                                            .getJSONArray("zones");
                int currentBossCount = 0;
                int currentHardCount = 0;
                int currentMediumCount = 0;
                int currentEasyCount = 0;
                for (int j = 0; j < 96; j++) {
                    JSONObject zone = zones.getJSONObject(j);
                    if (!zone.getBoolean("captured")) {
                        switch (zone.getInt("difficulty")) {
                            case 4:
                                currentBossCount++; break;
                            case 3:
                                currentHardCount++; break;
                            case 2:
                                currentMediumCount++; break;
                            case 1:
                                currentEasyCount++; break;
                        }
                    }
                }
                if (currentBossCount > highestBossCount) {
                    highestBossCount = currentBossCount;
                    highestBossCountIndex = i;
                }
                if (currentHardCount > highestHardCount) {
                    highestHardCount = currentHardCount;
                    highestHardCountIndex = i;
                }
                if (currentMediumCount > highestMediumCount) {
                    highestMediumCount = currentMediumCount;
                    highestMediumCountIndex = i;
                }
                if (currentEasyCount > highestEasyCount) {
                    highestEasyCount = currentEasyCount;
                    highestEasyCountIndex = i;
                }
            }

            int bestPlanet = 0;
            if (highestBossCount > 0) {
                bestPlanet = highestBossCountIndex;
                Log.d("Saliens", "index " + bestPlanet + " has " + highestBossCount + " bosses");
            } else if (highestHardCount > 0) {
                bestPlanet = highestHardCountIndex;
                Log.d("Saliens", "index " + bestPlanet + " has " + highestHardCount + " hard");
            } else if (highestMediumCount > 0) {
                bestPlanet = highestMediumCountIndex;
                Log.d("Saliens", "index " + bestPlanet + " has " + highestMediumCount + " medium");
            } else if (highestEasyCount > 0) {
                bestPlanet = highestEasyCountIndex;
                Log.d("Saliens", "index " + bestPlanet + " has " + highestEasyCount + " easy");
            } else {
                Log.d("Saliens", "wtf there are no high ones");
            }

            double bestTilePercent = 1;
            int bestTileDifficulty = 0;
            int bestTile = 0;

            JSONObject planetInfo = new JSONObject(tileData.get(bestPlanet));
            JSONArray zones = planetInfo.getJSONObject("response")
                    .getJSONArray("planets")
                    .getJSONObject(0)
                    .getJSONArray("zones");

            for (int i = 0; i < 96; i++) {
                JSONObject zone = zones.getJSONObject(i);
                int diff = zone.getInt("difficulty");
                double percent = zone.getDouble("capture_progress");
                boolean captured = zone.getBoolean("captured");
                if (diff > bestTileDifficulty && !captured) {
                    bestTileDifficulty = diff;
                    bestTilePercent = percent;
                    bestTile = i;
                }
                if (diff >= bestTileDifficulty && !captured &&
                    percent < bestTilePercent) {
                    bestTileDifficulty = diff;
                    bestTilePercent = percent;
                    bestTile = i;
                }
            }

            int bestPlanetID = planetInfo.getJSONObject("response")
                    .getJSONArray("planets")
                    .getJSONObject(0)
                    .getInt("id");

            int currentPlanetID = playerData.getInt("active_planet");

            SetupNotificationAndAttack(planetData, tileData, bestPlanet, bestPlanetID, currentPlanetID, bestTile);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void SetupNotificationAndAttack(JSONArray planetData, ArrayList<String> tileData, int bestPlanet, int bestPlanetID, int currentPlanetID, int bestTile) {
        if (!notificationOn) {
            notificationManager = NotificationManagerCompat.from(this);
            builder = new NotificationCompat.Builder(this, "saliens_time");
            builder.setContentTitle("Saliens Android")
                   .setContentText("Searching for planets...")
                   .setSmallIcon(R.drawable.ic_game)
                   .setOnlyAlertOnce(true)
                   .setPriority(NotificationCompat.PRIORITY_LOW);
            if (android.os.Build.VERSION.SDK_INT >= 26) {
                NotificationChannel mChannel = new NotificationChannel("saliens_time_not", "Saliens Client", NotificationManager.IMPORTANCE_LOW);
                NotificationManager oreoNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                oreoNotificationManager.createNotificationChannel(mChannel);
                builder.setChannelId("saliens_time_not");
            }

            builder.setProgress(110, 0, false);
            notificationId = Integer.parseInt(new SimpleDateFormat("ddHHmmssS", Locale.US).format(new Date()));
            notificationManager.notify(notificationId, builder.build());

            notificationOn = true;
        } else {
            builder.setProgress(110, 0, false);
            notificationManager.notify(notificationId, builder.build());
        }

        try {
            JSONObject planetInfo = new JSONObject(tileData.get(bestPlanet));
            JSONObject state = planetInfo.getJSONObject("response")
                    .getJSONArray("planets")
                    .getJSONObject(0)
                    .getJSONObject("state");
            int diff = planetInfo.getJSONObject("response")
                    .getJSONArray("planets")
                    .getJSONObject(0)
                    .getJSONArray("zones")
                    .getJSONObject(bestTile)
                    .getInt("difficulty");

            builder.setContentTitle("On " + state.getString("name") + " | " + level + " (" + scoreCur + "/" + scoreNex + ")")
                   .setContentText("Tile [" + ((bestTile%12)+1) + "/" + (((int)Math.floor(bestTile/12))+1) + "]");

            Timer testTimer = new Timer();
            testTimer.schedule(new NotificationTimerTask(testTimer, notificationManager, builder, notificationId, bestPlanet, bestPlanetID, currentPlanetID, bestTile, diff, this), 0, 1000);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class NotificationTimerTask extends TimerTask {
        private Timer timer;
        private NotificationManagerCompat manager;
        private NotificationCompat.Builder builder;
        private int id;
        private int bestPlanet;
        private int bestPlanetID;
        private int currentPlanetID;
        private int bestTile;
        private int diff;
        private MainActivity ma;

        public NotificationTimerTask(Timer timer, NotificationManagerCompat manager, NotificationCompat.Builder builder, int id, int bestPlanet, int bestPlanetID, int currentPlanetID, int bestTile, int diff, MainActivity ma) {
            this.timer = timer;
            this.manager = manager;
            this.builder = builder;
            this.id = id;
            this.bestPlanet = bestPlanet;
            this.bestPlanetID = bestPlanetID;
            this.currentPlanetID = currentPlanetID;
            this.bestTile = bestTile;
            this.diff = diff;
            this.ma = ma;
        }

        private int i = 0;

        @Override
        public void run() {
            if (i == 0) { //first tick
                try {
                    if (currentPlanetID != bestPlanetID) {
                        Post("ITerritoryControlMinigameService/JoinPlanet",
                                "access_token=" + token +
                                      "&id=" + bestPlanetID);
                    }
                    Post("ITerritoryControlMinigameService/JoinZone",
                            "access_token=" + token +
                                  "&zone_position=" + bestTile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (i < 110) {
                        builder.setProgress(110, i, false);
                        manager.notify(id, builder.build());
                    }
                    if (i == 110) {
                        builder.setContentText("Loading!")
                               .setProgress(0,0,false);
                        manager.notify(id, builder.build());
                    }
                }
            });
            if (i == 120) {
                try {
                    String data = Post("ITerritoryControlMinigameService/ReportScore",
                            "access_token=" + token +
                                  "&score=" + GetScore(diff) +
                                  "&language=english");
                    Log.d("Saliens","Sending " + "access_token=" + token +
                            "&score=" + GetScore(diff) +
                            "&language=english");
                    Log.d("Saliens", "Getting " + data);
                    if (data.equals("{\"response\":{}}")) {
                        Log.d("Saliens","Failed to send... rip");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                timer.cancel();
                new MainActivity.RestartAndGetNewInfo(ma).execute();
                timer.purge();
            }
            i++;
        }
    }

    public String GetScore(int diff) {
        switch (diff) {
            case 1:
                return "600";
            case 2:
                return "1200";
            case 3:
            case 4:
                return "2400";
        }
        return "600";
    }

    public static String Get(String path, String info) throws IOException {
        URL url = new URL("https://community.steam-api.com/" + path + "/v0001/?" + info);
        URLConnection urlConnection = url.openConnection();
        HttpsURLConnection httpConnection = (HttpsURLConnection)urlConnection;
        httpConnection.setRequestMethod("GET");
        httpConnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");
        httpConnection.setRequestProperty("Accept", "*/*");
        httpConnection.setRequestProperty("Origin", "https://steamcommunity.com");
        httpConnection.setRequestProperty("Referer", "https://steamcommunity.com/saliengame/play");
        httpConnection.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3464.0 Safari/537.36");
        BufferedReader in = new BufferedReader(new InputStreamReader(httpConnection.getInputStream(), "UTF8"));
        if (httpConnection.getResponseCode() != 200)
            return null;
        StringBuilder result = new StringBuilder();
        String line;
        while ((line = in.readLine()) != null) {
            result.append(line);
        }
        return result.toString();
    }

    public static String Post(String path, String info) throws IOException {
        URL url = new URL("https://community.steam-api.com/" + path + "/v0001/");
        Log.d("Saliens-post","https://community.steam-api.com/" + path + "/v0001/?"+info);
        URLConnection urlConnection = url.openConnection();
        HttpsURLConnection httpConnection = (HttpsURLConnection)urlConnection;
        httpConnection.setRequestMethod("POST");
        httpConnection.setDoOutput(true);
        httpConnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");
        httpConnection.setRequestProperty("Accept", "*/*");
        httpConnection.setRequestProperty("Origin", "https://steamcommunity.com");
        httpConnection.setRequestProperty("Referer", "https://steamcommunity.com/saliengame/play");
        httpConnection.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3464.0 Safari/537.36");
        DataOutputStream out = new DataOutputStream(httpConnection.getOutputStream());
        out.write(info.getBytes(StandardCharsets.UTF_8));
        BufferedReader in = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
        if (httpConnection.getResponseCode() != 200)
            return null;
        StringBuilder result = new StringBuilder();
        String line;
        while ((line = in.readLine()) != null) {
            result.append(line);
        }
        return result.toString();
    }
}
