## Saliens on Android
I may be a bit late to the botting party, but here it is anyway. Just open the app, paste the json from http://steamcommunity.com/saliengame/gettoken and restart the app. Then just leave the app open (it doesn't have to be in the foreground, just don't close it) and your alien will fight for you. You can also check out the active planets and the zones that have been captured along with the games available.

Note: the app is somewhat unstable at this time. Here are some bugs:

* If you post incorrect json when the app first installs, the app may crash after restarting (just uninstall and reinstall)
* Sometimes the background task will suddenly crash after the game is finished and may not finish for a couple minutes
* The planet's map does not update until you restart the app (not really a bug but whatever)
* The notifcation will show again if you try to swipe it out without first closing the app